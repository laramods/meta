<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('metas', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('object_type');
            $table->string('object_id');
            $table->string('key');
            $table->longText('value')->nullable();

            $table->unique(['object_type', 'object_id', 'key']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metas');
    }
}
