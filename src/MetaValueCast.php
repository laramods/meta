<?php

namespace Laramods\Meta;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class MetaValueCast implements CastsAttributes
{


    /**
     * @inheritDoc
     * @property Meta $model
     */
    public function get($model, string $key, $value, array $attributes)
    {
        if( $attributes['type'] ?? false ){
            return MetaHelpers::unserialize( $value, $attributes['type'] );
        }

        return $value;
    }

    /**
     * @inheritDoc
     * @property Meta $model
     */
    public function set($model, string $key, $value, array $attributes)
    {
        return MetaHelpers::serialize( $value );
    }
}