<?php

namespace Laramods\Meta\Providers;


use Illuminate\Support\ServiceProvider;

class MetaServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');
        $this->publishes([ __DIR__.'/../../migrations' => database_path('migrations') ], ['laramods-meta-migrations']);

    }


    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {



    }


}
