<?php

namespace Laramods\Meta;

use Illuminate\Database\Eloquent\Collection;

class MetaCollection extends Collection
{

    /**
     * Converts Meta Collection to an Array.
     * @return array
     */
    public function toArray()
    {

        $array = [];
        foreach ( $this->items as $item ){
            $array[$item->key] = $item->value;
        }

        return $array;

    }


    /**
     * @param int|string $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return $this->where('key', $offset)->count() > 0;
    }


    /**
     * @param int|string $offset
     * @return mixed
     */
    public function offsetGet($offset): mixed {
        return $this->where('key', $offset)->first->value->value ?? null;
    }


    /**
     * @param int|string $offset
     * @param mixed $value
     * @return void
     * @throws \Exception
     */
    public function offsetSet($offset, $value): void{
        throw new \Exception('Cannot set meta value this way. Use pushMetas() instead.');
    }


    /**
     * @param int|string $offset
     * @return void
     * @throws \Exception
     */
    public function offsetUnset($offset): void{
        throw new \Exception('Cannot set meta value this way. Use pushMetas() instead.');
    }

}
