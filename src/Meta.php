<?php

namespace Laramods\Meta;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Laramods\Meta\Traits\HasMeta;

/**
 * @property int $id
 * @property string $key
 * @property string $value
 * @property string $type
 * @property HasMeta $object
 */
class Meta extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'key', 'value', 'type',
    ];

    protected $casts = [
        'value' => MetaValueCast::class,
    ];


    // Relations ========================


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function object(){
        return $this->morphTo();
    }


    /**
     * @param array $models
     * @return MetaCollection
     */
    public function newCollection(array $models = [])
    {
        return new MetaCollection( $models );
    }


//    public function toArray()
//    {
//        $array = parent::toArray();
//
//        if( isset( $array['value'] ) && isset( $array['type'] ) ){
//            $array['value'] = MetaHelpers::unserialize($array['value'], $array['type']);
//            dd( $array );
//        }
//
//        return $array;
//    }

}
