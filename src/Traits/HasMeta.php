<?php

namespace Laramods\Meta\Traits;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Laramods\Meta\Meta;
use Laramods\Meta\MetaCollection;
use Laramods\Meta\MetaHelpers;

/**
 * Trait HasMeta
 * @package MVCommerceModules\Meta
 *
 * @property array|MetaCollection metas
 * @property Collection $meta Meta data in key => value format. Setting this variable will replace set the value in database automatically.
 */
trait HasMeta
{


    /** @var MetaCollection */
    private $__cached_meta;


    public static function bootHasMeta(){

        /**
         * Just before deleting the model, delete all metas
         * associated with the model forst. One can combine
         * this with transaction to make sure things happen
         * correctly.
         */

        static::deleting(function($model){
            /** @var HasMeta $model */
            $model->meta()->delete();
        });
    }


    // Relations ========================

    /**
     * @return MorphMany
     */
    public function meta(){
        return $this->morphMany( Meta::class, 'object' );
    }



    // Scopes ===========================

    public function scopeMetaQuery(Builder $query, array $metaQuery){

        $query->whereHas( 'meta', function(Builder $query) use ($metaQuery){

            $query->where(function(Builder $query) use ($metaQuery){

                foreach ($metaQuery as $item){

                    if( count($item) === 2 ){
                        list($key, $value) = $item;
                        $operator = is_scalar($value) ? '=' : 'in';
                    }else{
                        list($key, $operator, $value) = $item;
                    }

                    $query->orWhere(function(Builder $query) use ($key, $operator, $value){

                        // Key is always conditioned.
                        $query->where('key', $key);

                        // QUERY: WHERE IN
                        if( $operator === 'in' ){
                            $query->whereIn('value', (array) $value);

                        // QUERY: BETWEEN
                        }else if( $operator === '><' || $operator === 'between' ){
                            $query->whereBetween('value', (array) $value);

                        // QUERY: NOT BETWEEN
                        }else if( $operator === '<>' || $operator === '!between' || $operator === 'not-between' ){
                            $query->whereNotBetween('value', (array) $value);

                        // QUERY: ALL OTHER GENERIC CONDITIONS
                        }else{
                            $query->where('value', $operator, $value);
                        }
                    });

                }

            });

            $query->groupBy('object_id');
            $query->select(DB::raw('COUNT(*)'));
            $query->having(DB::raw('COUNT(*)'), '>=', count($metaQuery));

        });
    }



    // Methods =========================

    private function __fetchMetaCacheFromDB(){
        return $this->__cached_meta = $this->meta()->get(['value', 'key', 'type']);
    }


    // Accessors =======================


    public function getMetasAttribute(){

        if( $this->__cached_meta ){
            return $this->__cached_meta;
        }

        return $this->__fetchMetaCacheFromDB();

    }


    // Mutators =======================

    /**
     * @param array|Arrayable $values
     * @return Collection
     */
    public function setMetasAttribute($values){

        $values = MetaHelpers::prepareMetaArray( $values );

        DB::transaction(function() use ($values){
            // Create only that are required and delete only that are required, update otherwise.
            $keys = array_map(fn($value) => $value['key'], $values);

            // Remove keys that are not being updated.
            $this->meta()->whereNotIn('key', $keys)->delete();


            // Update existing or create new.
            foreach ( $values as $value ){
                $this->meta()->updateOrCreate([
                    'key' => $value['key'],
                ], [
                    'value' => $value['value'],
                    'type' => $value['type'],
                ]);
            }

        }, 3);

        return $this->__fetchMetaCacheFromDB();

    }


    /**
     * Push metas to database, without removing any existing that
     * are not present in $values but replaces if coincides.
     *
     * @param Arrayable|array $values
     */
    public function pushMetas($values){
        $values = MetaHelpers::prepareMetaArray( $values );

        foreach ( $values as $value ){
            $this->meta()->updateOrCreate([
                'key' => $value['key'],
            ], [
                'value' => $value['value'],
                'type' => $value['type'],
            ]);
        }
        return $this->__fetchMetaCacheFromDB();
    }

}
