<?php

namespace Laramods\Meta;

use Illuminate\Contracts\Support\Arrayable;

class MetaHelpers
{


    public static function serialize($value){

        $type = 'unknown';

        if( is_scalar( $value ) ){

            if( is_string( $value ) ){
                $type = 'string';
            }else if( is_int( $value ) ){
                $type = 'int';
            }else if( is_float( $value ) ){
                $type = 'float';
            }else if( is_double( $value ) ){
                $type = 'float';
            }else if( is_bool( $value ) ){
                $type = 'bool';
                $value = intval( $value );
            }

        }else{

            if( is_array( $value ) ){
                $type = 'array';
                $value = json_encode( $value );
            }else if( $value instanceof Arrayable ){
                $type = 'array';
                $value = json_encode( $value->toArray() );
            }else if( is_object( $value ) ){
                $type = 'object';
                $value = serialize( $value );
            }else if( is_null( $value ) ){
                $type = 'null';
                $value = '';
            }

        }

        if( $type === 'unknown' ){
            $value = serialize( $value );
        }

        return compact('value', 'type');

    }


    /**
     * @param $value
     * @param $type
     * @return bool|float|int|array|object|mixed
     */
    public static function unserialize($value, $type){

        switch ( $type ){
            case '':
            case 'unknown':
            case 'object':
                return unserialize( $value );

            case 'array':
                return json_decode( $value, true );

            case 'bool':
                return !! intval( $value );

            case 'float':
                return floatval( $value );

            case 'int':
                return intval( $value );

            case 'null':
                return null;

            default:
                return $value;
        }

    }


    /**
     * Prepares the meta array, serializing and getting
     * the type of meta, from array of values.
     *
     * @param Arrayable|array $values
     * @return array
     */
    public static function prepareMetaArray($values){

        if( $values instanceof Arrayable ){
            $values = $values->toArray();
        }

        $new_values = [];
        array_walk($values, function ($value, $key) use (&$new_values){
            $serialized = MetaHelpers::serialize( $value );
            $new_values[ $key ] = array_merge($serialized, compact('key'));
        });

        return array_values( $new_values );
    }

}